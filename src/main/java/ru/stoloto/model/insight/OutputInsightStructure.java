package ru.stoloto.model.insight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutputInsightStructure {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private Integer type;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getType() {
        return type;
    }

}