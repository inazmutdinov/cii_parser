package ru.stoloto.init.config;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import ru.stoloto.model.insight.OutputInsightStructure;

import java.io.File;

public class CiiGroupCodeInsightConfig implements InsightConfig {

    private static final String CONFIG_URL = String.format("%s/insight_dictionary_CII_GROUP", CONFIG_DIR);

    private static int insightObjectTypeId;

    private static String insightIdAttrName;
    private static String ciiGroupCodeAttrName;
    private static String groupAttributeAttrName;
    private static String createdAttrName;
    private static String updatedAttrName;
    private static String sourceSystemAttrName;
    private static String integrationNameAttrName;
    private static String sourceProtocolAttrName;
    private static String sourceFormatAttrName;
    private static String integrationScenarioAttrName;
    private static String scheduleAttrName;
    private static String boiAttrName;
    private static String boiFormatAttrName;
    private static String confluenceUrlAttrName;
    private static String commentAttrName;
    private static String protocolDescriptionAttrName;

    static {
        try {
            Parameters params = new Parameters();
            File propertiesFile = new File(CONFIG_URL);

            FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                    new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                            .configure(params.fileBased()
                                    .setFile(propertiesFile).setEncoding("UTF-8"));

            Configuration config = builder.getConfiguration();

            insightObjectTypeId = config.getInt("insight_object_type_id");
            insightIdAttrName = config.getString("insight_id");
            ciiGroupCodeAttrName = config.getString("cii_group_code");
            groupAttributeAttrName = config.getString("group_attribute");
            createdAttrName = config.getString("created");
            updatedAttrName = config.getString("updated");
            sourceSystemAttrName = config.getString("source_system");
            integrationNameAttrName = config.getString("integration_name");
            sourceProtocolAttrName = config.getString("source_protocol");
            sourceFormatAttrName = config.getString("source_format");
            integrationScenarioAttrName = config.getString("integration_scenario");
            scheduleAttrName = config.getString("schedule");
            boiAttrName = config.getString("boi");
            boiFormatAttrName = config.getString("boi_format");
            confluenceUrlAttrName = config.getString("confluence_url");
            commentAttrName = config.getString("comment");
            protocolDescriptionAttrName = config.getString("protocol_description");

            System.out.println("Insight ID's config for CII_GROUP_code was read successfully");
        } catch (ConfigurationException cex) {
            System.out.println("Error while reading insight ID's config for CII_GROUP_code");
            cex.printStackTrace();
            System.exit(0);
        }

    }

    @Override
    public int getInsightObjectTypeId() {
        return insightObjectTypeId;
    }

    @Override
    public CiiGroupCodeAttributeId attributeId(OutputInsightStructure[] codesInsight) {
        return new CiiGroupCodeAttributeId(codesInsight);
    }

    public static class CiiGroupCodeAttributeId implements AttributeId {

        private int insightIdAttrId;
        private int ciiGroupCodeAttrId;
        private int groupAttributeAttrId;
        private int createdAttrId;
        private int updatedAttrId;
        private int sourceSystemAttrId;
        private int integrationNameAttrId;
        private int sourceProtocolAttrId;
        private int sourceFormatAttrId;
        private int integrationScenarioAttrId;
        private int scheduleAttrId;
        private int boiAttrId;
        private int boiFormatAttrId;
        private int confluenceUrlAttrId;
        private int commentAttrId;
        private int protocolDescriptionAttrId;

        public int getInsightObjectTypeId() {
            return insightObjectTypeId;
        }

        public int getInsightIdAttrId() {
            return insightIdAttrId;
        }

        public int getCiiGroupCodeAttrId() {
            return ciiGroupCodeAttrId;
        }

        public int getGroupAttributeAttrId() {
            return groupAttributeAttrId;
        }

        public int getCreatedAttrId() {
            return createdAttrId;
        }

        public int getUpdatedAttrId() {
            return updatedAttrId;
        }

        public int getSourceSystemAttrId() {
            return sourceSystemAttrId;
        }

        public int getIntegrationNameAttrId() {
            return integrationNameAttrId;
        }

        public int getSourceProtocolAttrId() {
            return sourceProtocolAttrId;
        }

        public int getSourceFormatAttrId() {
            return sourceFormatAttrId;
        }

        public int getIntegrationScenarioAttrId() {
            return integrationScenarioAttrId;
        }

        public int getScheduleAttrId() {
            return scheduleAttrId;
        }

        public int getBoiAttrId() {
            return boiAttrId;
        }

        public int getBoiFormatAttrId() {
            return boiFormatAttrId;
        }

        public int getConfluenceUrlAttrId() {
            return confluenceUrlAttrId;
        }

        public int getCommentAttrId() {
            return commentAttrId;
        }

        public int getProtocolDescriptionAttrId() {
            return protocolDescriptionAttrId;
        }

        @Override
        public int getCiiCodeAttrId() {
            return 0;
        }

        @Override
        public int getStatusAttrId() {
            return 0;
        }

        @Override
        public int getChangeInProgressAttrId() {
            return 0;
        }

        @Override
        public int getTemporaryAttrId() {
            return 0;
        }

        @Override
        public int getDateToProdAttrId() {
            return 0;
        }

        @Override
        public int getConsumerSystemAttrId() {
            return 0;
        }

        @Override
        public int getConsumerProtocolAttrId() {
            return 0;
        }

        @Override
        public int getConsumerFormatAttrId() {
            return 0;
        }

        @Override
        public int getWithEsbAttrId() {
            return 0;
        }

        @Override
        public int getModeAttrId() {
            return 0;
        }

        @Override
        public int getMethodQueryMessIdAttrId() {
            return 0;
        }

        public CiiGroupCodeAttributeId(OutputInsightStructure[] codesInsight) {
            for (OutputInsightStructure outputInsightStructure : codesInsight) {
                String attrName = outputInsightStructure.getName();
                int attrId = outputInsightStructure.getId();

                if (insightIdAttrName.equals(attrName)) {
                    insightIdAttrId = attrId;
                } else if (ciiGroupCodeAttrName.equals(attrName)) {
                    ciiGroupCodeAttrId = attrId;
                } else if (groupAttributeAttrName.equals(attrName)) {
                    groupAttributeAttrId = attrId;
                } else if (createdAttrName.equals(attrName)) {
                    createdAttrId = attrId;
                } else if (updatedAttrName.equals(attrName)) {
                    updatedAttrId = attrId;
                } else if (sourceSystemAttrName.equals(attrName)) {
                    sourceSystemAttrId = attrId;
                } else if (integrationNameAttrName.equals(attrName)) {
                    integrationNameAttrId = attrId;
                } else if (sourceProtocolAttrName.equals(attrName)) {
                    sourceProtocolAttrId = attrId;
                } else if (sourceFormatAttrName.equals(attrName)) {
                    sourceFormatAttrId = attrId;
                } else if (integrationScenarioAttrName.equals(attrName)) {
                    integrationScenarioAttrId = attrId;
                } else if (scheduleAttrName.equals(attrName)) {
                    scheduleAttrId = attrId;
                } else if (boiAttrName.equals(attrName)) {
                    boiAttrId = attrId;
                } else if (boiFormatAttrName.equals(attrName)) {
                    boiFormatAttrId = attrId;
                } else if (confluenceUrlAttrName.equals(attrName)) {
                    confluenceUrlAttrId = attrId;
                } else if (commentAttrName.equals(attrName)) {
                    commentAttrId = attrId;
                } else if (protocolDescriptionAttrName.equals(attrName)) {
                    protocolDescriptionAttrId = attrId;
                } else {
                    System.out.println("CII_GROUP. Cannot find field: " + outputInsightStructure.getName());
                }
            }
        }

    }

}
