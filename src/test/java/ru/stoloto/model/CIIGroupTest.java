package ru.stoloto.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CIIGroupTest {

    @Test
    void generateCiiGroupCodeFromCii() {
        String ciiCode = "OUTCC_SAPCRM.9.3.3.1";
        Assertions.assertEquals("OUTCC.9.3.X.1", CIIGroup.generateCiiGroupCodeFromCii(ciiCode));
    }

}