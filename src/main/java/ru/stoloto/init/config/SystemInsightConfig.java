package ru.stoloto.init.config;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import ru.stoloto.model.insight.OutputInsightStructure;

import java.io.File;

public class SystemInsightConfig implements InsightConfig {

    private static final String CONFIG_URL = String.format("%s/insight_dictionary_SYSTEM", CONFIG_DIR);

    private static int insightObjectTypeId;

    private static String insightIdAttrName;
    private static String systemAttrName;

    static {
        try {
            Parameters params = new Parameters();
            File propertiesFile = new File(CONFIG_URL);

            FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                    new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                            .configure(params.fileBased()
                                    .setFile(propertiesFile).setEncoding("UTF-8"));

            Configuration config = builder.getConfiguration();

            insightObjectTypeId = config.getInt("insight_object_type_id");
            insightIdAttrName = config.getString("insight_id");
            systemAttrName = config.getString("system");

            System.out.println("Insight ID's config for SYSTEM was read successfully");
        } catch (ConfigurationException cex) {
            System.out.println("Error while reading insight ID's config for SYSTEM");
            cex.printStackTrace();
            System.exit(0);
        }

    }

    @Override
    public int getInsightObjectTypeId() {
        return insightObjectTypeId;
    }

    @Override
    public SystemAttributeId attributeId(OutputInsightStructure[] insights) {
        return new SystemAttributeId(insights);
    }

    public static class SystemAttributeId implements AttributeId {

        private int insightId;
        private int systemAttrId;

        public int getInsightObjectTypeId() {
            return insightObjectTypeId;
        }

        public int getInsightId() {
            return insightId;
        }

        public int getSystemAttrId() {
            return systemAttrId;
        }

        public SystemAttributeId(OutputInsightStructure[] codesInsight) {
            for (OutputInsightStructure outputInsightStructure : codesInsight) {
                String attrName = outputInsightStructure.getName();
                int attrId = outputInsightStructure.getId();

                if (insightIdAttrName.equals(attrName)) {
                    insightId = attrId;
                } else if (systemAttrName.equals(attrName)) {
                    systemAttrId = attrId;
                } else {
                    System.out.println("SYSTEM. Cannot find field: " + outputInsightStructure.getName());
                }
            }
        }

        @Override
        public int getInsightIdAttrId() {
            return 0;
        }

        @Override
        public int getCiiGroupCodeAttrId() {
            return 0;
        }

        @Override
        public int getGroupAttributeAttrId() {
            return 0;
        }

        @Override
        public int getCreatedAttrId() {
            return 0;
        }

        @Override
        public int getUpdatedAttrId() {
            return 0;
        }

        @Override
        public int getSourceSystemAttrId() {
            return 0;
        }

        @Override
        public int getIntegrationNameAttrId() {
            return 0;
        }

        @Override
        public int getSourceProtocolAttrId() {
            return 0;
        }

        @Override
        public int getSourceFormatAttrId() {
            return 0;
        }

        @Override
        public int getIntegrationScenarioAttrId() {
            return 0;
        }

        @Override
        public int getScheduleAttrId() {
            return 0;
        }

        @Override
        public int getBoiAttrId() {
            return 0;
        }

        @Override
        public int getBoiFormatAttrId() {
            return 0;
        }

        @Override
        public int getConfluenceUrlAttrId() {
            return 0;
        }

        @Override
        public int getCommentAttrId() {
            return 0;
        }

        @Override
        public int getProtocolDescriptionAttrId() {
            return 0;
        }

        @Override
        public int getCiiCodeAttrId() {
            return 0;
        }

        @Override
        public int getStatusAttrId() {
            return 0;
        }

        @Override
        public int getChangeInProgressAttrId() {
            return 0;
        }

        @Override
        public int getTemporaryAttrId() {
            return 0;
        }

        @Override
        public int getDateToProdAttrId() {
            return 0;
        }

        @Override
        public int getConsumerSystemAttrId() {
            return 0;
        }

        @Override
        public int getConsumerProtocolAttrId() {
            return 0;
        }

        @Override
        public int getConsumerFormatAttrId() {
            return 0;
        }

        @Override
        public int getWithEsbAttrId() {
            return 0;
        }

        @Override
        public int getModeAttrId() {
            return 0;
        }

        @Override
        public int getMethodQueryMessIdAttrId() {
            return 0;
        }

    }

}
