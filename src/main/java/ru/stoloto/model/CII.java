package ru.stoloto.model;

import ru.stoloto.init.config.AttributeId;
import ru.stoloto.model.insight.ObjectTypeInsightStructure;

public interface CII {

    ObjectTypeInsightStructure convertToOutputInsightStructure(AttributeId attributeId);

    String getInsightId();

    String getSelfLink();

    String getBoiFormat();

    void setBoiFormat(String s);

    int defineIntegrationScenario();

}
