package ru.stoloto.init.config;

public interface AttributeId {

    int getInsightObjectTypeId();

    int getInsightIdAttrId();

    int getCiiGroupCodeAttrId();

    int getGroupAttributeAttrId();

    int getCreatedAttrId();

    int getUpdatedAttrId();

    int getSourceSystemAttrId();

    int getIntegrationNameAttrId();

    int getSourceProtocolAttrId();

    int getSourceFormatAttrId();

    int getIntegrationScenarioAttrId();

    int getScheduleAttrId();

    int getBoiAttrId();

    int getBoiFormatAttrId();

    int getConfluenceUrlAttrId();

    int getCommentAttrId();

    int getProtocolDescriptionAttrId();

    int getCiiCodeAttrId();

    int getStatusAttrId();

    int getChangeInProgressAttrId();

    int getTemporaryAttrId();

    int getDateToProdAttrId();

    int getConsumerSystemAttrId();

    int getConsumerProtocolAttrId();

    int getConsumerFormatAttrId();

    int getWithEsbAttrId();

    int getModeAttrId();

    int getMethodQueryMessIdAttrId();

}
