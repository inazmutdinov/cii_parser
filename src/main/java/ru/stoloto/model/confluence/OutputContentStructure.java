package ru.stoloto.model.confluence;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OutputContentStructure {

    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("ancestors")
    @Expose
    public List<Ancestor> ancestors = null;
    @SerializedName("space")
    @Expose
    public Space space;
    @SerializedName("body")
    @Expose
    public Body body;

    public OutputContentStructure(String title, List<Ancestor> ancestors, Space space, Body body) {
        this.type = "page";
        this.title = title;
        this.ancestors = ancestors;
        this.space = space;
        this.body = body;
    }

    public static class Ancestor {

        @SerializedName("id")
        @Expose
        public Integer id;

        public Ancestor(Integer id) {
            this.id = id;
        }

    }

    public static class Body {

        @SerializedName("storage")
        @Expose
        public Storage storage;

        public Body(Storage storage) {
            this.storage = storage;
        }

    }

    public static class Space {

        @SerializedName("key")
        @Expose
        public String key;

        public Space(String key) {
            this.key = key;
        }

    }

    public static class Storage {

        @SerializedName("value")
        @Expose
        public String value;
        @SerializedName("representation")
        @Expose
        public String representation;

        public Storage(String value) {
            this.value = value;
            this.representation = "storage";
        }

    }

}