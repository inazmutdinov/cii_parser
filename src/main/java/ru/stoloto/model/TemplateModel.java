package ru.stoloto.model;

import ru.stoloto.dictionary.TemplateAttributeOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TemplateModel {

    String ciiCodeValue;
    String integrationName;
    String sourceSystem;
    String responsibleSource;
    String consumerSystem;
    String responsibleConsumer;
    String initiator;
    String withEsb;
    String externalIntegration;
    String sourceProtocol;
    String sourceFormat;
    String consumerProtocol;
    String consumerFormat;
    String mode;
    String integrationScenario;
    String scheduleSource;
    String scheduleConsumer;
    String targetPayload;
    String maxPayload;
    String comment;
    String dateToProd;
    String esbType;
    String status;
    String sourceSystemInsightId;
    String consumerSystemInsightId;

    public String getCiiCodeValue() {
        return ciiCodeValue;
    }

    public String getIntegrationName() {
        return integrationName;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public String getResponsibleSource() {
        return responsibleSource;
    }

    public String getConsumerSystem() {
        return consumerSystem;
    }

    public String getResponsibleConsumer() {
        return responsibleConsumer;
    }

    public String getInitiator() {
        return initiator;
    }

    public String getWithEsb() {
        return withEsb;
    }

    public String getExternalIntegration() {
        return externalIntegration;
    }

    public String getSourceProtocol() {
        return sourceProtocol;
    }

    public String getSourceFormat() {
        return sourceFormat;
    }

    public String getConsumerProtocol() {
        return consumerProtocol;
    }

    public String getConsumerFormat() {
        return consumerFormat;
    }

    public String getMode() {
        return mode;
    }

    public String getIntegrationScenario() {
        return integrationScenario;
    }

    public String getScheduleSource() {
        return scheduleSource;
    }

    public String getScheduleConsumer() {
        return scheduleConsumer;
    }

    public String getTargetPayload() {
        return targetPayload;
    }

    public String getMaxPayload() {
        return maxPayload;
    }

    public String getComment() {
        return comment;
    }

    public String getDateToProd() {
        return dateToProd;
    }

    public String getEsbType() {
        return esbType;
    }

    public String getStatus() {
        return status;
    }

    public String getSourceSystemInsightId() {
        return sourceSystemInsightId;
    }

    public String getConsumerSystemInsightId() {
        return consumerSystemInsightId;
    }

    public void setSourceSystemInsightId(String sourceSystemInsightId) {
        this.sourceSystemInsightId = sourceSystemInsightId;
    }

    public void setConsumerSystemInsightId(String consumerSystemInsightId) {
        this.consumerSystemInsightId = consumerSystemInsightId;
    }

    public TemplateModel(Map<Integer, List<String>> data, int rowId) {
        this.ciiCodeValue = data.get(rowId).get(TemplateAttributeOrder.CII_CODE.id);
        this.integrationName = data.get(rowId).get(TemplateAttributeOrder.INTEGRATION_NAME.id);
        this.sourceSystem = data.get(rowId).get(TemplateAttributeOrder.SOURCE_SYSTEM.id);
        this.responsibleSource = data.get(rowId).get(TemplateAttributeOrder.RESPONSIBLE_SOURCE.id); //dont used
        this.consumerSystem = data.get(rowId).get(TemplateAttributeOrder.CONSUMER_SYSTEM.id);
        this.responsibleConsumer = data.get(rowId).get(TemplateAttributeOrder.RESPONSIBLE_CONSUMER.id); //dont used
        this.initiator = data.get(rowId).get(TemplateAttributeOrder.INITIATOR.id); // dont used
        this.withEsb = data.get(rowId).get(TemplateAttributeOrder.WITH_ESB.id);
        this.externalIntegration = data.get(rowId).get(TemplateAttributeOrder.EXTERNAL_INTEGRATION.id);
        this.sourceProtocol = data.get(rowId).get(TemplateAttributeOrder.SOURCE_PROTOCOL.id);
        this.sourceFormat = data.get(rowId).get(TemplateAttributeOrder.SOURCE_FORMAT.id);
        this.consumerProtocol = data.get(rowId).get(TemplateAttributeOrder.CONSUMER_PROTOCOL.id);
        this.consumerFormat = data.get(rowId).get(TemplateAttributeOrder.CONSUMER_FORMAT.id);
        this.mode = data.get(rowId).get(TemplateAttributeOrder.MODE.id);
        this.integrationScenario = data.get(rowId).get(TemplateAttributeOrder.INTEGRATION_SCENARIO.id);
        this.scheduleSource = data.get(rowId).get(TemplateAttributeOrder.SCHEDULE_SOURCE.id);
        this.scheduleConsumer = data.get(rowId).get(TemplateAttributeOrder.SCHEDULE_CONSUMER.id); // for CII group
        this.targetPayload = data.get(rowId).get(TemplateAttributeOrder.TARGET_PAYLOAD.id);// dont used
        this.maxPayload = data.get(rowId).get(TemplateAttributeOrder.MAX_PAYLOAD.id);// dont used
        this.comment = data.get(rowId).get(TemplateAttributeOrder.COMMENT.id);
        this.dateToProd = convertDateToInsightFormat(data.get(rowId).get(TemplateAttributeOrder.DATE_TO_PROD.id));
        setDefaultValues();
    }

    /**
     * Convert date from cii template to correct insight format
     *
     * @param inputDate String from template
     * @return String in insight format
     */
    private String convertDateToInsightFormat(String inputDate) {
        if (inputDate == null || "".equals(inputDate)) {
            return "";
        }
        Date date = null;
        String templatePattern = "EEE MMM dd HH:mm:ss z yyyy";
        String insightPattern = "dd.MM.yyyy HH:mm";
        try {
            date = new SimpleDateFormat(templatePattern, Locale.US).parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat(insightPattern).format(date);
    }

    /**
     * Set custom values without template
     */
    private void setDefaultValues() {
        status = "".equals(ciiCodeValue) ? "Планируемое" : "Промышленное";
        esbType = "Да".equals(withEsb) ? "Шина IDF" : "Без использования Шины";
    }

}
