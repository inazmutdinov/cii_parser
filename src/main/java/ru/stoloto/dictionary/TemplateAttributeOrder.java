package ru.stoloto.dictionary;

public enum TemplateAttributeOrder {
    CII_CODE(0),
    INTEGRATION_NAME(1),
    SOURCE_SYSTEM(2),
    RESPONSIBLE_SOURCE(3),
    CONSUMER_SYSTEM(4),
    RESPONSIBLE_CONSUMER(5),
    INITIATOR(6),
    WITH_ESB(7),
    EXTERNAL_INTEGRATION(8),
    SOURCE_PROTOCOL(9),
    SOURCE_FORMAT(10),
    CONSUMER_PROTOCOL(11),
    CONSUMER_FORMAT(12),
    MODE(13),
    INTEGRATION_SCENARIO(14),
    SCHEDULE_SOURCE(15),
    SCHEDULE_CONSUMER(16),
    TARGET_PAYLOAD(17),
    MAX_PAYLOAD(18),
    DATE_TO_PROD(19),
    COMMENT(20);

    public final int id;

    TemplateAttributeOrder(int id) {
        this.id = id;
    }

}
