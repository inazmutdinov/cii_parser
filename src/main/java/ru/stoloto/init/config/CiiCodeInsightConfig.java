package ru.stoloto.init.config;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import ru.stoloto.model.insight.OutputInsightStructure;

import java.io.File;

public class CiiCodeInsightConfig implements InsightConfig {

    private static final String CONFIG_URL = String.format("%s/insight_dictionary_CII_CODE", CONFIG_DIR);

    private static int insightObjectTypeId;

    private static String insightIdAttrName;
    private static String ciiGroupCodeAttrName;
    private static String ciiCodeAttrName;
    private static String groupAttributeAttrName;
    private static String statusAttrName;
    private static String temporaryAttrName;
    private static String changeInProgressAttrName;
    private static String dateToProdAttrName;
    private static String createdAttrName;
    private static String updatedAttrName;
    private static String sourceSystemAttrName;
    private static String consumerSystemAttrName;
    private static String integrationNameAttrName;
    private static String sourceProtocolAttrName;
    private static String sourceFormatAttrName;
    private static String consumerProtocolAttrName;
    private static String consumerFormatAttrName;
    private static String withEsbAttrName;
    private static String modeAttrName;
    private static String integrationScenarioAttrName;
    private static String scheduleAttrName;
    private static String boiAttrName;
    private static String boiFormatAttrName;
    private static String confluenceUrlAttrName;
    private static String commentAttrName;
    private static String protocolDescriptionAttrName;
    private static String methodQueryMessIdAttrName;

    static {
        try {
            Parameters params = new Parameters();
            File propertiesFile = new File(CONFIG_URL);

            FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                    new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                            .configure(params.fileBased()
                                    .setFile(propertiesFile).setEncoding("UTF-8"));

            Configuration config = builder.getConfiguration();

            insightObjectTypeId = config.getInt("insight_object_type_id");
            insightIdAttrName = config.getString("insight_id");
            ciiGroupCodeAttrName = config.getString("cii_group_code");
            ciiCodeAttrName = config.getString("cii_code");
            groupAttributeAttrName = config.getString("group_attribute");
            statusAttrName = config.getString("status");
            temporaryAttrName = config.getString("temporary");
            changeInProgressAttrName = config.getString("change_in_progress");
            dateToProdAttrName = config.getString("date_to_prod");
            createdAttrName = config.getString("created");
            updatedAttrName = config.getString("updated");
            sourceSystemAttrName = config.getString("source_system");
            consumerSystemAttrName = config.getString("consumer_system");
            integrationNameAttrName = config.getString("integration_name");
            sourceProtocolAttrName = config.getString("source_protocol");
            sourceFormatAttrName = config.getString("source_format");
            consumerProtocolAttrName = config.getString("consumer_protocol");
            consumerFormatAttrName = config.getString("consumer_format");
            withEsbAttrName = config.getString("with_esb");
            modeAttrName = config.getString("mode");
            integrationScenarioAttrName = config.getString("integration_scenario");
            scheduleAttrName = config.getString("schedule");
            boiAttrName = config.getString("boi");
            boiFormatAttrName = config.getString("boi_format");
            confluenceUrlAttrName = config.getString("confluence_url");
            commentAttrName = config.getString("comment");
            protocolDescriptionAttrName = config.getString("protocol_description");
            methodQueryMessIdAttrName = config.getString("method_query_mess_id");

            System.out.println("Insight ID's config for CII_code was read successfully");
        } catch (ConfigurationException cex) {
            System.out.println("Error while reading insight ID's config for CII_code");
            cex.printStackTrace();
            System.exit(0);
        }

    }

    @Override
    public int getInsightObjectTypeId() {
        return insightObjectTypeId;
    }

    @Override
    public CiiCodeAttributeId attributeId(OutputInsightStructure[] codesInsight) {
        return new CiiCodeAttributeId(codesInsight);
    }

    public static class CiiCodeAttributeId implements AttributeId {

        private int insightIdAttrId;
        private int ciiGroupCodeAttrId;
        private int ciiCodeAttrId;
        private int groupAttributeAttrId;
        private int statusAttrId;
        private int temporaryAttrId;
        private int changeInProgressAttrId;
        private int dateToProdAttrId;
        private int createdAttrId;
        private int updatedAttrId;
        private int sourceSystemAttrId;
        private int consumerSystemAttrId;
        private int integrationNameAttrId;
        private int sourceProtocolAttrId;
        private int sourceFormatAttrId;
        private int consumerProtocolAttrId;
        private int consumerFormatAttrId;
        private int withEsbAttrId;
        private int modeAttrId;
        private int integrationScenarioAttrId;
        private int scheduleAttrId;
        private int boiAttrId;
        private int boiFormatAttrId;
        private int confluenceUrlAttrId;
        private int commentAttrId;
        private int protocolDescriptionAttrId;
        private int methodQueryMessIdAttrId;

        public int getInsightObjectTypeId() {
            return insightObjectTypeId;
        }

        public int getInsightIdAttrId() {
            return insightIdAttrId;
        }

        public int getCiiGroupCodeAttrId() {
            return ciiGroupCodeAttrId;
        }

        public int getCiiCodeAttrId() {
            return ciiCodeAttrId;
        }

        public int getGroupAttributeAttrId() {
            return groupAttributeAttrId;
        }

        public int getStatusAttrId() {
            return statusAttrId;
        }

        public int getTemporaryAttrId() {
            return temporaryAttrId;
        }

        public int getChangeInProgressAttrId() {
            return changeInProgressAttrId;
        }

        public int getDateToProdAttrId() {
            return dateToProdAttrId;
        }

        public int getCreatedAttrId() {
            return createdAttrId;
        }

        public int getUpdatedAttrId() {
            return updatedAttrId;
        }

        public int getSourceSystemAttrId() {
            return sourceSystemAttrId;
        }

        public int getConsumerSystemAttrId() {
            return consumerSystemAttrId;
        }

        public int getIntegrationNameAttrId() {
            return integrationNameAttrId;
        }

        public int getSourceProtocolAttrId() {
            return sourceProtocolAttrId;
        }

        public int getSourceFormatAttrId() {
            return sourceFormatAttrId;
        }

        public int getConsumerProtocolAttrId() {
            return consumerProtocolAttrId;
        }

        public int getConsumerFormatAttrId() {
            return consumerFormatAttrId;
        }

        public int getWithEsbAttrId() {
            return withEsbAttrId;
        }

        public int getModeAttrId() {
            return modeAttrId;
        }

        public int getIntegrationScenarioAttrId() {
            return integrationScenarioAttrId;
        }

        public int getScheduleAttrId() {
            return scheduleAttrId;
        }

        public int getBoiAttrId() {
            return boiAttrId;
        }

        public int getBoiFormatAttrId() {
            return boiFormatAttrId;
        }

        public int getConfluenceUrlAttrId() {
            return confluenceUrlAttrId;
        }

        public int getCommentAttrId() {
            return commentAttrId;
        }

        public int getProtocolDescriptionAttrId() {
            return protocolDescriptionAttrId;
        }

        public int getMethodQueryMessIdAttrId() {
            return methodQueryMessIdAttrId;
        }

        public CiiCodeAttributeId(OutputInsightStructure[] codesInsight) {
            for (OutputInsightStructure outputInsightStructure : codesInsight) {
                String attrName = outputInsightStructure.getName();
                int attrId = outputInsightStructure.getId();

                if (insightIdAttrName.equals(attrName)) {
                    insightIdAttrId = attrId;
                } else if (ciiGroupCodeAttrName.equals(attrName)) {
                    ciiGroupCodeAttrId = attrId;
                } else if (ciiCodeAttrName.equals(attrName)) {
                    ciiCodeAttrId = attrId;
                } else if (groupAttributeAttrName.equals(attrName)) {
                    groupAttributeAttrId = attrId;
                } else if (statusAttrName.equals(attrName)) {
                    statusAttrId = attrId;
                } else if (temporaryAttrName.equals(attrName)) {
                    temporaryAttrId = attrId;
                } else if (changeInProgressAttrName.equals(attrName)) {
                    changeInProgressAttrId = attrId;
                } else if (dateToProdAttrName.equals(attrName)) {
                    dateToProdAttrId = attrId;
                } else if (createdAttrName.equals(attrName)) {
                    createdAttrId = attrId;
                } else if (updatedAttrName.equals(attrName)) {
                    updatedAttrId = attrId;
                } else if (sourceSystemAttrName.equals(attrName)) {
                    sourceSystemAttrId = attrId;
                } else if (consumerSystemAttrName.equals(attrName)) {
                    consumerSystemAttrId = attrId;
                } else if (integrationNameAttrName.equals(attrName)) {
                    integrationNameAttrId = attrId;
                } else if (sourceProtocolAttrName.equals(attrName)) {
                    sourceProtocolAttrId = attrId;
                } else if (sourceFormatAttrName.equals(attrName)) {
                    sourceFormatAttrId = attrId;
                } else if (consumerProtocolAttrName.equals(attrName)) {
                    consumerProtocolAttrId = attrId;
                } else if (consumerFormatAttrName.equals(attrName)) {
                    consumerFormatAttrId = attrId;
                } else if (withEsbAttrName.equals(attrName)) {
                    withEsbAttrId = attrId;
                } else if (modeAttrName.equals(attrName)) {
                    modeAttrId = attrId;
                } else if (integrationScenarioAttrName.equals(attrName)) {
                    integrationScenarioAttrId = attrId;
                } else if (scheduleAttrName.equals(attrName)) {
                    scheduleAttrId = attrId;
                } else if (boiAttrName.equals(attrName)) {
                    boiAttrId = attrId;
                } else if (boiFormatAttrName.equals(attrName)) {
                    boiFormatAttrId = attrId;
                } else if (confluenceUrlAttrName.equals(attrName)) {
                    confluenceUrlAttrId = attrId;
                } else if (commentAttrName.equals(attrName)) {
                    commentAttrId = attrId;
                } else if (protocolDescriptionAttrName.equals(attrName)) {
                    protocolDescriptionAttrId = attrId;
                } else if (methodQueryMessIdAttrName.equals(attrName)) {
                    methodQueryMessIdAttrId = attrId;
                } else {
                    System.out.println("CII_CODE. Cannot find field: " + outputInsightStructure.getName());
                }
            }
        }

    }

}
