package ru.stoloto.init.config;

import ru.stoloto.model.insight.OutputInsightStructure;

public interface InsightConfig {

    int getInsightObjectTypeId();

    String CONFIG_DIR = "./config";

    AttributeId attributeId(OutputInsightStructure[] codesInsight);

}
