package ru.stoloto;

import com.jayway.jsonpath.JsonPath;
import ru.stoloto.init.config.Config;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

public class InsightWorker {

    private static final String CREATE_ISSUE_URL = String.format("%s/object/create", Config.getInsightUrl());
    private static final String UPDATE_ISSUE_URL = String.format("%s/object", Config.getInsightUrl());
    private static final String FIND_IQL_URL = String.format("%s/iql/objects?objectSchemaId=%s&iql=", Config.getInsightUrl(), Config.getObjectSchemaId());
    private static final String OBJECT_TYPE_URL = String.format("%s/objecttype/<object_type>/attributes", Config.getInsightUrl());
    private static final String CONTENT_CONFLUENCE_URL = String.format("%s/content", Config.getConfluenceUrl());
    private static final String FIND_CQL_URL = String.format("%s/search?cql=", CONTENT_CONFLUENCE_URL);

    /**
     * Create new object in Insight. More: https://insight-javadoc.riada.io/insight-javadoc-8.6/insight-rest/#object
     *
     * @param jsonInput String json as Insight structure
     */
    public static void createIssue(String jsonInput) {
        try {
            HttpURLConnection con = getConnectionWithBasicAuth(CREATE_ISSUE_URL, "POST", Config.getUser(), Config.getPassword());
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInput.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            int responseCode = con.getResponseCode();
            if (responseCode < 400) {
                System.out.println(String.format("Created. Response code: %s", responseCode));
            } else {
                System.out.println(String.format("Error while sending request to %s. Response code: %s", CREATE_ISSUE_URL, responseCode));
                System.out.println(String.format("Request: %s", jsonInput));
                System.out.println(String.format("Response: %s", getBodyFromStream(con.getErrorStream())));
            }
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public static void updateIssue(String jsonInput, String insightId) {
        String url = String.format("%s/%s", UPDATE_ISSUE_URL, insightId);
        try {
            HttpURLConnection con = getConnectionWithBasicAuth(url, "PUT", Config.getUser(), Config.getPassword());
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInput.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            int responseCode = con.getResponseCode();
            if (responseCode < 400) {
                System.out.println(String.format("Updated. Response code: %s", responseCode));
            } else {
                System.out.println(String.format("Error while sending request to %s. Response code: %s", url, responseCode));
                System.out.println(String.format("Request: %s", jsonInput));
                System.out.println(String.format("Response: %s", getBodyFromStream(con.getErrorStream())));
            }
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    /**
     * Get attribute structure from Insight. More: https://insight-javadoc.riada.io/insight-javadoc-8.6/insight-rest/#objecttypeattribute
     * Call {@link #getFromInsightByUrl(String)}
     *
     * @param objectTypeId int object type id
     * @return String structure or null, if error occurred
     */
    public static String getObjectTypeAttributes(int objectTypeId) {
        String urlString = OBJECT_TYPE_URL.replaceAll("<object_type>", String.valueOf(objectTypeId));
        String response = null;
        try {
            response = getFromInsightByUrl(urlString);
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            System.out.println(String.format("Input object type id: %s", objectTypeId));
        }
        return response;
    }

    /**
     * Search in Insight by IQL. More: https://insight-javadoc.riada.io/insight-javadoc-8.6/insight-rest/#iql
     * Call {@link #getFromInsightByUrl(String)}
     *
     * @param searchLabelName label for search
     * @return String result of searching or null, if error occurred
     */
    public static String findByIql(String searchLabelName, int objectTypeId) {
        String iql = String.format("label=\"%s\" and objectTypeId=\"%s\"", searchLabelName, objectTypeId);
        String urlString = String.format("%s%s", FIND_IQL_URL, URLEncoder.encode(iql, StandardCharsets.UTF_8));
        String response = null;
        try {
            response = getFromInsightByUrl(urlString);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(String.format("Input search label: %s", searchLabelName));
        }
        return response;
    }

    /**
     * One point for working with GET verbs to Insight
     *
     * @param urlString url for calling
     * @return String result
     * @throws IOException exception
     */
    private static String getFromInsightByUrl(String urlString) throws IOException {
        String response = null;
        HttpURLConnection con = getConnectionWithBasicAuth(urlString, "GET", Config.getUser(), Config.getPassword());
        int responseCode = con.getResponseCode();
        if (responseCode < 400) {
            response = getBodyFromStream(con.getInputStream());
        } else {
            System.out.println(String.format("Error while sending request to %s. Response code: %s", urlString, responseCode));
            System.out.println(String.format("Response: %s", getBodyFromStream(con.getErrorStream())));
        }
        return response;
    }

    /**
     * Find searchValue of some attribute. Call {@link #findByIql(String, int)}
     *
     * @param searchLabelName String label for search
     * @param attributeId     ind attribute id
     * @return value of "searchValue" element or null, if there are no result, or more then 1
     */
    public static String findByIql(String searchLabelName, int objectTypeId, int attributeId) {
        String response = findByIql(searchLabelName, objectTypeId);
        if (response == null) {
            return null;
        }
        int pageSize = JsonPath.read(response, "$.pageSize");
        if (pageSize == 0) {
            System.out.println(String.format("Where no objects found for %s", searchLabelName));
            return null;
        }
        if (pageSize > 1) {
            System.out.println(String.format("Found more then 1 result (%s) for %s", pageSize, searchLabelName));
            return null;
        }
        List<String> insightId = JsonPath.read(response, genJPathInsight(attributeId));
        return pasteValueOrDummy(insightId);
    }

    public static String createConfluencePage(String jsonInput) {
        String response = null;
        try {
            HttpURLConnection con = getConnectionWithBasicAuth(CONTENT_CONFLUENCE_URL, "POST", Config.getUser(), Config.getPassword());
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInput.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            int responseCode = con.getResponseCode();
            if (responseCode < 400) {
                response = getBodyFromStream(con.getInputStream());
            } else {
                System.out.println(String.format("Error while sending request to %s. Response code: %s", CONTENT_CONFLUENCE_URL, responseCode));
                System.out.println(String.format("Request: %s", jsonInput));
                System.out.println(String.format("Response: %s", getBodyFromStream(con.getErrorStream())));
            }
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return JsonPath.read(response, genJPathConfluencePageId());
    }

    //TODO переделать к хренам
    public static String findByCql(String title, String space) {
        String cql = String.format("title=\"%s\" and space=\"%s\" and type=page", title, space);
        String urlString = String.format("%s%s", FIND_CQL_URL, URLEncoder.encode(cql, StandardCharsets.UTF_8));
        String response = null;
        try {
            HttpURLConnection con = getConnectionWithBasicAuth(urlString, "GET", Config.getUser(), Config.getPassword());
            int responseCode = con.getResponseCode();
            if (responseCode < 400) {
                response = getBodyFromStream(con.getInputStream());
            } else {
                System.out.println(String.format("Error while sending request to %s. Response code: %s", urlString, responseCode));
                System.out.println(String.format("Response: %s", getBodyFromStream(con.getErrorStream())));
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error.");
        }
        int pageSize = JsonPath.read(response, "$.size");
        if (pageSize == 0) {
            System.out.println(String.format("Where no objects found for %s", title));
            return null;
        }
        if (pageSize > 1) {
            System.out.println(String.format("Found more then 1 result (%s) for %s", pageSize, title));
            return null;
        }
        return JsonPath.read(response, "$.results[0].id");
    }

    /**
     * Create new HttpURLConnection by input parameters
     *
     * @param urlString URL as String
     * @param method    HTTP verb
     * @param user      user for Basic Auth
     * @param password  password for Basic Auth
     * @return prepared connection
     * @throws IOException exception
     */
    private static HttpURLConnection getConnectionWithBasicAuth(String urlString, String method, String user, String password) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        String encodedLogPass = new String(Base64.getEncoder().encode(String.format("%s:%s", user, password).getBytes()));
        connection.setRequestProperty("Authorization", String.format("Basic %s", encodedLogPass));
        return connection;
    }

    /**
     * Generate JPath to searchValue by it attribute id
     *
     * @param attributeId int value of attribute id
     * @return JPath as String
     */
    private static String genJPathInsight(int attributeId) {
        return "$.objectEntries[0]" +
                ".attributes[?(@.objectTypeAttributeId==" + attributeId + ")]" +
                ".objectAttributeValues[0]" +
                ".searchValue";
    }

    /**
     * Generate JPath for extract id of Confluence page
     *
     * @return JPath as String
     */
    private static String genJPathConfluencePageId() {
        return "$.id";
    }

    /**
     * Analyze list. If empty - return "". Else return first value
     *
     * @param list input list
     * @return "" or String value first element
     */
    private static String pasteValueOrDummy(List<String> list) {
        return list.isEmpty() ? "" : list.get(0);
    }

    /**
     * Method for reading data from inputStream
     *
     * @param inputStream input
     * @return Strinng with result of reading
     * @throws IOException exception
     */
    public static String getBodyFromStream(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(inputStream));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        return content.toString();
    }

}
