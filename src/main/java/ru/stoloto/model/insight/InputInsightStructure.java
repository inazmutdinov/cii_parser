package ru.stoloto.model.insight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InputInsightStructure {

    @SerializedName("objectEntries")
    @Expose
    public List<ObjectEntry> objectEntries = null;
    @SerializedName("pageSize")
    @Expose
    public Integer pageSize;

    public class ObjectEntry {

        @SerializedName("objectKey")
        @Expose
        public String objectKey;
        @SerializedName("attributes")
        @Expose
        public List<Attribute> attributes = null;
        @SerializedName("_links")
        @Expose
        public Link link;

    }

    public class Attribute {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("objectTypeAttributeId")
        @Expose
        public Integer objectTypeAttributeId;
        @SerializedName("objectAttributeValues")
        @Expose
        public List<ObjectAttributeValue> objectAttributeValues = null;
        @SerializedName("objectId")
        @Expose
        public Integer objectId;

    }

    public class ObjectAttributeValue {

        @SerializedName("value")
        @Expose
        public String value;
        @SerializedName("searchValue")
        @Expose
        public String searchValue;
        @SerializedName("referencedType")
        @Expose
        public Boolean referencedType;
        @SerializedName("displayValue")
        @Expose
        public String displayValue;

    }

    public class Link {

        @SerializedName("self")
        @Expose
        public String self;

    }

}
