package ru.stoloto.model.insight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ObjectTypeInsightStructure {

    @SerializedName("objectTypeId")
    @Expose
    private Integer objectTypeId;
    @SerializedName("attributes")
    @Expose
    private List<Attribute> attributes = null;

    public Integer getObjectTypeId() {
        return objectTypeId;
    }

    public void setObjectTypeId(Integer objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public ObjectTypeInsightStructure(Integer objectTypeId, List<Attribute> attributes) {
        this.objectTypeId = objectTypeId;
        this.attributes = attributes;
    }

    public static class Attribute {

        @SerializedName("objectTypeAttributeId")
        @Expose
        private Integer objectTypeAttributeId;
        @SerializedName("objectAttributeValues")
        @Expose
        private List<ObjectAttributeValue> objectAttributeValues = null;

        public Integer getObjectTypeAttributeId() {
            return objectTypeAttributeId;
        }

        public void setObjectTypeAttributeId(Integer objectTypeAttributeId) {
            this.objectTypeAttributeId = objectTypeAttributeId;
        }

        public List<ObjectAttributeValue> getObjectAttributeValues() {
            return objectAttributeValues;
        }

        public void setObjectAttributeValues(List<ObjectAttributeValue> objectAttributeValues) {
            this.objectAttributeValues = objectAttributeValues;
        }

        public Attribute(Integer objectTypeAttributeId, List<ObjectAttributeValue> objectAttributeValues) {
            this.objectTypeAttributeId = objectTypeAttributeId;
            this.objectAttributeValues = objectAttributeValues;
        }

    }

    public static class ObjectAttributeValue {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public ObjectAttributeValue(String value) {
            this.value = value;
        }

    }

}
