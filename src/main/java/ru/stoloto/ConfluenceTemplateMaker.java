package ru.stoloto;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.stoloto.dictionary.ConfluencePageTypes;
import ru.stoloto.init.config.Config;
import ru.stoloto.init.config.ConfluenceFormatConfig;
import ru.stoloto.model.confluence.OutputContentStructure;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfluenceTemplateMaker {

    private static final String EXCEL_LINE_SEPARATOR = "\n";

    public static OutputContentStructure getStructureForConfluencePage(Map<Integer, List<String>> dataFromTemplate, String pageName, String ciiObjectUrl, String parentPage, ConfluencePageTypes type) {
        Map<Object, Object> root = new HashMap<>();
        List<String> headers = new ArrayList<>();
        List<List<String>> rows = new ArrayList<>();

        if (dataFromTemplate != null && !dataFromTemplate.isEmpty()) {
            for (int i = 0; i < Config.getFormatTemplateColumnCount(); i++) {
                headers.add(dataFromTemplate.get(1).get(i));
            }
            for (int i = 2; i < dataFromTemplate.size(); i++) {
                List<String> row = new ArrayList<>();
                try {
                    for (int j = 0; j < Config.getFormatTemplateColumnCount(); j++) {
                        row.add(dataFromTemplate.get(i).get(j));
                    }
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    continue;
                }
                rows.add(row);
            }
        }

        String firstCellForTableHeader;
        try {
            firstCellForTableHeader = dataFromTemplate == null ? "" : dataFromTemplate.get(0).get(0);
        } catch (Exception e) {
            firstCellForTableHeader = "";
        }

        String[] strings = firstCellForTableHeader.split(EXCEL_LINE_SEPARATOR);
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < strings.length; i++) {
            sb.append(strings[i]);
            sb.append(EXCEL_LINE_SEPARATOR);
        }

        Template temp = null;
        String tableName = strings[0];
        switch (type) {
            case PARENT:
                if ("".equals(pageName)) {
                    pageName = tableName;
                }
                temp = ConfluenceFormatConfig.getTemplate();
                break;
            case CHILDREN:
                pageName = pageName + " " + tableName;
                temp = ConfluenceFormatConfig.getChildTemplate();
                break;
        }

        root.put("insight_object_url", ciiObjectUrl);
        root.put("cii_value", pageName);
        root.put("table_name", tableName);
        root.put("table_description", sb.toString());

        root.put("headers", headers);
        root.put("rows", rows);

        /* Merge data-model with template */
        Writer out = new StringWriter();
        try {
            temp.process(root, out);
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }

        List<OutputContentStructure.Ancestor> list = new ArrayList<>();
        list.add(new OutputContentStructure.Ancestor(Integer.parseInt(parentPage)));

        return new OutputContentStructure(pageName, list, new OutputContentStructure.Space(Config.getConfluenceSpaceKey()), new OutputContentStructure.Body(new OutputContentStructure.Storage(out.toString())));
    }

}
