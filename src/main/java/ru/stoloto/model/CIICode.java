package ru.stoloto.model;

import ru.stoloto.init.config.AttributeId;
import ru.stoloto.model.insight.InputInsightStructure;
import ru.stoloto.model.insight.ObjectTypeInsightStructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CIICode implements CII {

    private static final boolean GROUP_ATTRIBUTE = false;

    private String insightId;
    private String ciiGroupCodeInsightId;
    private String ciiCodeValue;
    private String status;
    private boolean temporary;
    private boolean changeInProgress;
    private String dateToProd;
    private String created;
    private String updated;
    private String sourceSystem;
    private String consumerSystem;
    private String integrationName;
    private String sourceProtocol;
    private String sourceFormat;
    private String consumerProtocol;
    private String consumerFormat;
    private String withEsb;
    private String mode;
    private String integrationScenario;
    private String schedule;
    private String boi;
    private String boiFormat;
    private String confluenceUrl;
    private String comment;
    private String protocolDescription;
    private String methodQueryMessId;
    private String selfLink;

    public CIICode(String insightId, String ciiGroupCodeInsightId, String ciiCodeValue, String status, boolean temporary, boolean changeInProgress, String dateToProd, String sourceSystem, String consumerSystem, String integrationName, String sourceProtocol, String sourceFormat, String consumerProtocol, String consumerFormat, String withEsb, String mode, String integrationScenario, String schedule, String boi, String boiFormat, String confluenceUrl, String comment, String protocolDescription, String methodQueryMessId) {
        this.insightId = insightId;
        this.ciiGroupCodeInsightId = ciiGroupCodeInsightId;
        this.ciiCodeValue = ciiCodeValue;
        this.status = status;
        this.temporary = temporary;
        this.changeInProgress = changeInProgress;
        this.dateToProd = dateToProd;
        this.sourceSystem = sourceSystem;
        this.consumerSystem = consumerSystem;
        this.integrationName = integrationName;
        this.sourceProtocol = sourceProtocol;
        this.sourceFormat = sourceFormat;
        this.consumerProtocol = consumerProtocol;
        this.consumerFormat = consumerFormat;
        this.withEsb = withEsb;
        this.mode = mode;
        this.integrationScenario = integrationScenario;
        this.schedule = schedule;
        this.boi = boi;
        this.boiFormat = boiFormat;
        this.confluenceUrl = confluenceUrl;
        this.comment = comment;
        this.protocolDescription = protocolDescription;
        this.methodQueryMessId = methodQueryMessId;
    }

    public CIICode(InputInsightStructure inputInsightStructure, AttributeId attributeId) {
        this.selfLink = inputInsightStructure.objectEntries.get(0).link.self;
        for (InputInsightStructure.Attribute a : inputInsightStructure.objectEntries.get(0).attributes) {
            int attrIdFromJson = a.objectTypeAttributeId;
            if (attrIdFromJson == attributeId.getInsightIdAttrId()) {
                this.insightId = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getGroupAttributeAttrId()) {
                //Value GROUP_ATTRIBUTE is final
            } else if (attrIdFromJson == attributeId.getCiiGroupCodeAttrId()) {
                this.ciiGroupCodeInsightId = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getCiiCodeAttrId()) {
                this.ciiCodeValue = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getStatusAttrId()) {
                this.status = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getTemporaryAttrId()) {
                this.temporary = Boolean.getBoolean(a.objectAttributeValues.get(0).searchValue);
            } else if (attrIdFromJson == attributeId.getChangeInProgressAttrId()) {
                this.changeInProgress = Boolean.getBoolean(a.objectAttributeValues.get(0).searchValue);
            } else if (attrIdFromJson == attributeId.getDateToProdAttrId()) {
                this.dateToProd = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getSourceSystemAttrId()) {
                this.sourceSystem = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getConsumerSystemAttrId()) {
                this.consumerSystem = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getIntegrationNameAttrId()) {
                this.integrationName = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getSourceProtocolAttrId()) {
                this.sourceProtocol = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getSourceFormatAttrId()) {
                this.sourceFormat = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getConsumerProtocolAttrId()) {
                this.consumerProtocol = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getConsumerFormatAttrId()) {
                this.consumerFormat = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getWithEsbAttrId()) {
                this.withEsb = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getModeAttrId()) {
                this.mode = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getIntegrationScenarioAttrId()) {
                this.integrationScenario = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getScheduleAttrId()) {
                this.schedule = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getBoiAttrId()) {
                this.boi = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getBoiFormatAttrId()) {
                this.boiFormat = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getCommentAttrId()) {
                this.comment = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getProtocolDescriptionAttrId()) {
                this.protocolDescription = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getMethodQueryMessIdAttrId()) {
                this.methodQueryMessId = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getCreatedAttrId()) {
                this.created = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getUpdatedAttrId()) {
                this.updated = a.objectAttributeValues.get(0).searchValue;
            } else {
                System.out.println(String.format("Could not map attribute %s from Insight's json on any CII_CODE field", attrIdFromJson));
            }
        }
    }

    @Override
    public int defineIntegrationScenario() {
        String [] s = ciiCodeValue.split("\\.");
        return Integer.parseInt(s[1]);
    }

    public String getInsightId() {
        return insightId;
    }

    public String getCiiGroupCodeInsightId() {
        return ciiGroupCodeInsightId;
    }

    public String getBoiFormat() {
        return boiFormat;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setBoiFormat(String boiFormat) {
        this.boiFormat = boiFormat;
    }

    public ObjectTypeInsightStructure convertToOutputInsightStructure(AttributeId attributeId) {
        List<ObjectTypeInsightStructure.Attribute> attributes = new ArrayList<>();

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getCiiCodeAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(ciiCodeValue)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getCiiGroupCodeAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(ciiGroupCodeInsightId)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getGroupAttributeAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(String.valueOf(GROUP_ATTRIBUTE))))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getStatusAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(status)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getChangeInProgressAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(String.valueOf(changeInProgress))))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getTemporaryAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(String.valueOf(temporary))))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getDateToProdAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(dateToProd)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getSourceSystemAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(sourceSystem)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getConsumerSystemAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(consumerSystem)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getIntegrationNameAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(integrationName)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getSourceProtocolAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(sourceProtocol)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getSourceFormatAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(sourceFormat)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getConsumerProtocolAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(consumerProtocol)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getConsumerFormatAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(consumerFormat)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getWithEsbAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(withEsb)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getModeAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(mode)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getIntegrationScenarioAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(integrationScenario)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getScheduleAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(schedule)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getBoiAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(boi)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getBoiFormatAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(boiFormat)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getConfluenceUrlAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(confluenceUrl)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getCommentAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(comment)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getProtocolDescriptionAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(protocolDescription)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getMethodQueryMessIdAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(methodQueryMessId)))));

        return new ObjectTypeInsightStructure(attributeId.getInsightObjectTypeId(), attributes);
    }

}
