package ru.stoloto.model;

import ru.stoloto.init.config.AttributeId;
import ru.stoloto.model.insight.InputInsightStructure;
import ru.stoloto.model.insight.ObjectTypeInsightStructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CIIGroup implements CII {

    private static final boolean GROUP_ATTRIBUTE = true;

    private String insightId;
    private String ciiGroupCode;
    private String created;
    private String updated;
    private String sourceSystem;
    private String integrationName;
    private String sourceProtocol;
    private String sourceFormat;
    private String integrationScenario;
    private String schedule;
    private String boi;
    private String boiFormat;
    private String confluenceUrl;
    private String comment;
    private String protocolDescription;
    private String selfLink;

    public CIIGroup(String insightId, String ciiGroupCode, String sourceSystem, String integrationName, String sourceProtocol, String sourceFormat, String integrationScenario, String schedule, String boi, String boiFormat, String confluenceUrl, String comment, String protocolDescription) {
        this.insightId = insightId;
        this.ciiGroupCode = ciiGroupCode;
        this.sourceSystem = sourceSystem;
        this.integrationName = integrationName;
        this.sourceProtocol = sourceProtocol;
        this.sourceFormat = sourceFormat;
        this.integrationScenario = integrationScenario;
        this.schedule = schedule;
        this.boi = boi;
        this.boiFormat = boiFormat;
        this.confluenceUrl = confluenceUrl;
        this.comment = comment;
        this.protocolDescription = protocolDescription;
    }

    public CIIGroup(InputInsightStructure inputInsightStructure, AttributeId attributeId) {
        this.selfLink = inputInsightStructure.objectEntries.get(0).link.self;
        for (InputInsightStructure.Attribute a : inputInsightStructure.objectEntries.get(0).attributes) {
            int attrIdFromJson = a.objectTypeAttributeId;
            if (attrIdFromJson == attributeId.getInsightIdAttrId()) {
                this.insightId = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getGroupAttributeAttrId()) {
                //Value GROUP_ATTRIBUTE is final
            } else if (attrIdFromJson == attributeId.getCiiGroupCodeAttrId()) {
                this.ciiGroupCode = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getSourceSystemAttrId()) {
                this.sourceSystem = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getIntegrationNameAttrId()) {
                this.integrationName = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getSourceProtocolAttrId()) {
                this.sourceProtocol = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getSourceFormatAttrId()) {
                this.sourceFormat = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getIntegrationScenarioAttrId()) {
                this.integrationScenario = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getScheduleAttrId()) {
                this.schedule = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getBoiAttrId()) {
                this.boi = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getBoiFormatAttrId()) {
                this.boiFormat = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getCommentAttrId()) {
                this.comment = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getProtocolDescriptionAttrId()) {
                this.protocolDescription = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getCreatedAttrId()) {
                this.created = a.objectAttributeValues.get(0).searchValue;
            } else if (attrIdFromJson == attributeId.getUpdatedAttrId()) {
                this.updated = a.objectAttributeValues.get(0).searchValue;
            } else {
                System.out.println(String.format("Could not map attribute %s from Insight's json on any CII_GROUP_CODE field", attrIdFromJson));
            }
        }
    }

    @Override
    public int defineIntegrationScenario() {
        String[] s = ciiGroupCode.split("\\.");
        return Integer.parseInt(s[1]);
    }

    public String getInsightId() {
        return insightId;
    }

    public void setInsightId(String insightId) {
        this.insightId = insightId;
    }

    public String getBoiFormat() {
        return boiFormat;
    }

    public void setBoiFormat(String boiFormat) {
        this.boiFormat = boiFormat;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public ObjectTypeInsightStructure convertToOutputInsightStructure(AttributeId attributeId) {
        List<ObjectTypeInsightStructure.Attribute> attributes = new ArrayList<>();

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getCiiGroupCodeAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(ciiGroupCode)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getGroupAttributeAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(String.valueOf(GROUP_ATTRIBUTE))))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getSourceSystemAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(sourceSystem)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getIntegrationNameAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(integrationName)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getSourceProtocolAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(sourceProtocol)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getSourceFormatAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(sourceFormat)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getIntegrationScenarioAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(integrationScenario)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getScheduleAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(schedule)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getBoiAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(boi)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getBoiFormatAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(boiFormat)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getConfluenceUrlAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(confluenceUrl)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getCommentAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(comment)))));

        attributes.add(new ObjectTypeInsightStructure.Attribute(attributeId.getProtocolDescriptionAttrId(),
                new ArrayList<>(Collections.singletonList(
                        new ObjectTypeInsightStructure.ObjectAttributeValue(protocolDescription)))));

        return new ObjectTypeInsightStructure(attributeId.getInsightObjectTypeId(), attributes);
    }

    /**
     * Generate cii group code. Example "OUTCC.9.3.X.1"
     *
     * @param ciiCode format "OUTCC_SAPCRM.9.3.3.1"
     * @return string value of cii group code
     */
    public static String generateCiiGroupCodeFromCii(String ciiCode) {
        String[] arrByDot = ciiCode.split("\\.");
        String sourceSystemName = arrByDot[0].split("_")[0];
        return String.format("%s.%s.%s.X.%s", sourceSystemName, arrByDot[1], arrByDot[2], arrByDot[4]);
    }

}
