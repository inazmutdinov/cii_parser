package ru.stoloto.init.config;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import ru.stoloto.model.insight.OutputInsightStructure;

import java.io.File;
import java.io.IOException;

public class ConfluenceFormatConfig implements InsightConfig {

    private static Template template;
    private static Template childTemplate;

    static {
        /* Create and adjust the configuration singleton */
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);
        // Recommended settings for new projects:
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        cfg.setFallbackOnNullLoopVariable(false);
        try {
            cfg.setDirectoryForTemplateLoading(new File(CONFIG_DIR));
        } catch (IOException e) {
            System.out.println(String.format("Error reading '%s' dir", CONFIG_DIR));
            e.printStackTrace();
            System.exit(0);
        }
        try {
            template = cfg.getTemplate(Config.getConfluenceTemplateName());
            childTemplate = cfg.getTemplate(Config.getConfluenceChildTemplateName());
        } catch (IOException e) {
            System.out.println(String.format("Error while reading '%s' file", CONFIG_DIR + Config.getConfluenceTemplateName()));
            System.out.println(String.format("Error while reading '%s' file", CONFIG_DIR + Config.getConfluenceChildTemplateName()));
            e.printStackTrace();
            System.exit(0);
        }
    }

    public static Template getTemplate() {
        return template;
    }
    public static Template getChildTemplate() {
        return childTemplate;
    }

    @Override
    public int getInsightObjectTypeId() {
        throw new UnsupportedOperationException("Method is not supported");
    }

    @Override
    public AttributeId attributeId(OutputInsightStructure[] codesInsight) {
        throw new UnsupportedOperationException("Method is not supported");
    }

}
