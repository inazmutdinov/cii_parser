package ru.stoloto;

import com.google.gson.Gson;
import ru.stoloto.dictionary.ConfluencePageTypes;
import ru.stoloto.init.config.*;
import ru.stoloto.model.CII;
import ru.stoloto.model.CIICode;
import ru.stoloto.model.CIIGroup;
import ru.stoloto.model.TemplateModel;
import ru.stoloto.model.insight.InputInsightStructure;
import ru.stoloto.model.insight.OutputInsightStructure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.stoloto.ConfluenceTemplateMaker.getStructureForConfluencePage;

public class Main {

    private static String templateFilePath;

    private static Gson gson = new Gson();
    private static CiiCodeInsightConfig ciiCodeConfig = new CiiCodeInsightConfig();
    private static CiiGroupCodeInsightConfig ciiGroupConfig = new CiiGroupCodeInsightConfig();
    private static SystemInsightConfig systemConfig = new SystemInsightConfig();

    private static CiiCodeInsightConfig.CiiCodeAttributeId ciiCodeAttributeId = (CiiCodeInsightConfig.CiiCodeAttributeId) prepareConfig(ciiCodeConfig);
    private static CiiGroupCodeInsightConfig.CiiGroupCodeAttributeId ciiGroupAttrId = (CiiGroupCodeInsightConfig.CiiGroupCodeAttributeId) prepareConfig(ciiGroupConfig);
    private static SystemInsightConfig.SystemAttributeId systemAttrId = (SystemInsightConfig.SystemAttributeId) prepareConfig(systemConfig);

    public static void main(String[] args) {
        templateFilePath = args[0];
        Map<Integer, List<String>> data = null;
        try {
            data = TemplateParser.readTemplate(templateFilePath, Config.getTemplateParamSheetName());
        } catch (IOException e) {
            System.out.println("Cannot find main sheet");
            e.printStackTrace();
            System.exit(0);
        }
        for (int i = 1; i < data.size(); i++) {
            TemplateModel model = new TemplateModel(data, i);

            if ("".equals(model.getCiiCodeValue()) && "".equals(model.getIntegrationName())) {
                break;
            } else {
                System.out.println(String.format("Working with row №%s", i));
                model.setSourceSystemInsightId(checkSystem(model.getSourceSystem()));
                model.setConsumerSystemInsightId(checkSystem(model.getConsumerSystem()));

                String ciiGroupCode = CIIGroup.generateCiiGroupCodeFromCii(model.getCiiCodeValue());
                System.out.println(String.format("Generated CII_GROUP code: %s", ciiGroupCode));
                CIIGroup ciiGroup = (CIIGroup) prepareCii(ciiGroupCode, model, CIIGroup.class, ciiGroupConfig, ciiGroupAttrId);

                System.out.println(String.format("Working with CII_CODE: %s", model.getCiiCodeValue()));
                prepareCii(model.getCiiCodeValue(), model, CIICode.class, ciiCodeConfig, ciiCodeAttributeId, ciiGroup);
            }
            System.out.println(String.format("==============Finished working with record №%s, CII_CODE %s==============", i, model.getCiiCodeValue()));
        }
    }

    /**
     * Check the system in Insight. Exit program, if system is absent
     *
     * @param systemName name of system
     * @return String value of system Insight ID
     */
    private static String checkSystem(String systemName) {
        String systemInsightId = InsightWorker.findByIql(systemName, systemConfig.getInsightObjectTypeId(), systemAttrId.getInsightId());
        if (systemInsightId == null || "".equals(systemInsightId)) {
            System.out.println(String.format("SYSTEM %s was not found in Insight", systemName));
            System.exit(0);
        }
        System.out.println(String.format("Get systemId for %s: %s", systemName, systemInsightId));
        return systemInsightId;
    }

    /**
     * Trying get CII Group from Insight.
     *
     * @param ciiObjectCode Code of CII object
     * @param e             Class type
     * @param config        InsightConfig impl
     * @return CII object (CIIGroup or CIICode)
     */
    private static CII getCiiObjectFromInsight(String ciiObjectCode, Class<? extends CII> e, InsightConfig config, AttributeId attributeId) {
        String simpleNameForLog = e.getSimpleName();
        String ciiGroupResponse = InsightWorker.findByIql(ciiObjectCode, config.getInsightObjectTypeId());
        InputInsightStructure ciiObjectResponseStructure = gson.fromJson(ciiGroupResponse, InputInsightStructure.class);
        if (ciiObjectResponseStructure.pageSize > 1) {
            System.out.println(String.format("Too many objects found for %s %s", simpleNameForLog, ciiObjectCode));
            System.exit(0);
        } else if (ciiObjectResponseStructure.pageSize == 1) {
            System.out.println(String.format("%s %s was found in insight. Fetching.", simpleNameForLog, ciiObjectCode));
            if (e == CIIGroup.class) {
                return new CIIGroup(ciiObjectResponseStructure, attributeId);
            }
            if (e == CIICode.class) {
                return new CIICode(ciiObjectResponseStructure, attributeId);
            }
        }
        return null;
    }

    /**
     * Prepare attributeId from config
     *
     * @param insightConfig config
     * @return attr id
     */
    private static AttributeId prepareConfig(InsightConfig insightConfig) {
        String attrInsight = InsightWorker.getObjectTypeAttributes(insightConfig.getInsightObjectTypeId());
        OutputInsightStructure[] model = gson.fromJson(attrInsight, OutputInsightStructure[].class);
        return insightConfig.attributeId(model);
    }

    /**
     * Proxy {@link #prepareCii(String, TemplateModel, Class, InsightConfig, AttributeId, CIIGroup)} for working with CIIGroup
     *
     * @param ciiValue    String value of cii
     * @param model       Map with params from template
     * @param e           class type
     * @param config      config for CIIGroup or CIICode
     * @param attributeId AttributeId for CIIGroup or CIICode
     * @return result of {@link #prepareCii(String, TemplateModel, Class, InsightConfig, AttributeId, CIIGroup)}
     */
    private static CII prepareCii(String ciiValue, TemplateModel model, Class<? extends CII> e, InsightConfig config, AttributeId attributeId) {
        return prepareCii(ciiValue, model, e, config, attributeId, null);
    }

    /**
     * Work with CIIGroup or CIICode - check existing in Insight, create or update. Call method for work with format.
     *
     * @param ciiValue    String value of cii
     * @param model       Map with params from template
     * @param e           class type
     * @param config      config for CIIGroup or CIICode
     * @param attributeId AttributeId for CIIGroup or CIICode
     * @param ciiGroup    CIIGroup, if working with CIICode
     * @return prepared CIIGroup or CIICode
     */
    private static CII prepareCii(String ciiValue, TemplateModel model, Class<? extends CII> e, InsightConfig config, AttributeId attributeId, CIIGroup ciiGroup) {
        String simpleNameForLog = e.getSimpleName();
        CII ciiObj = getCiiObjectFromInsight(ciiValue, e, config, attributeId);
        if (ciiObj == null) {
            System.out.println(String.format("%s was not found in Insight. Start to create new one.", simpleNameForLog));
            if (e == CIIGroup.class) {
                ciiObj = new CIIGroup(
                        "",
                        ciiValue,
                        model.getSourceSystemInsightId(),
                        model.getIntegrationName(),
                        model.getSourceProtocol(),
                        model.getSourceFormat(),
                        model.getIntegrationScenario(),
                        model.getScheduleSource(),
                        "",
                        "",
                        "",
                        model.getComment(),
                        ""
                );
            } else if (e == CIICode.class) {
                ciiObj = new CIICode(
                        "",
                        ciiGroup == null ? "" : ciiGroup.getInsightId(),
                        ciiValue,
                        model.getStatus(),
                        false,
                        true,
                        model.getDateToProd(),
                        model.getSourceSystemInsightId(),
                        model.getConsumerSystemInsightId(),
                        model.getIntegrationName(),
                        model.getSourceProtocol(),
                        model.getSourceFormat(),
                        model.getConsumerProtocol(),
                        model.getConsumerFormat(),
                        model.getEsbType(),
                        model.getMode(),
                        model.getIntegrationScenario(),
                        model.getScheduleConsumer(),
                        "",
                        "",
                        "",
                        model.getComment(),
                        "",
                        ""
                );
            }
            InsightWorker.createIssue(gson.toJson(ciiObj.convertToOutputInsightStructure(attributeId)));
            System.out.println(String.format("Created new %s in Insight. One more request to insight for getting key", simpleNameForLog));
            ciiObj = getCiiObjectFromInsight(ciiValue, e, config, attributeId);
        }
        System.out.println(String.format("Got %s id for %s: %s", simpleNameForLog, ciiValue, ciiObj.getInsightId()));
        //TODO выбор метода обработки в зависимости от сценария обмена
        //TODO создавать пустой шаблон, если нет форматов
        checkAndFetchFormat(ciiValue, ciiObj, attributeId);

        return ciiObj;
    }

    /**
     * Identify scenario and choose one of method for process
     *
     * @param ciiValue    String value of cii code (cii group)
     * @param ciiObj      CIIGroup or CIICode
     * @param attributeId AttributeId for CIIGroup or CIICode
     */
    private static void checkAndFetchFormat(String ciiValue, CII ciiObj, AttributeId attributeId) {
        int scenario = ciiObj.defineIntegrationScenario();
        switch (scenario) {
            case 1:
            case 4:
                checkAndFetchFormatPackage(ciiValue, ciiObj, attributeId);
                break;
            default:
                checkAndFetchFormatSimple(ciiValue, ciiObj, attributeId);
        }
    }

    /**
     * Work with format. Check 1.Confluence 2.Template.
     * If 1, cii object will update with new url (the old link is not checked against the new one).
     *
     * @param ciiValue    String value of cii
     * @param ciiObj      CIIGroup or CIICode object
     * @param attributeId AttributeId for CIIGroup or CIICode
     */
    private static void checkAndFetchFormatSimple(String ciiValue, CII ciiObj, AttributeId attributeId) {
        String simpleNameForLog = ciiObj.getClass().getSimpleName();
        String confSearch = InsightWorker.findByCql(ciiValue, Config.getConfluenceSpaceKey());
        String ciiObjConfPageId = null;
        if (confSearch == null) {
            System.out.println(String.format("FORMAT for %s was not found in Confluence. Check template", ciiValue));
            try {
                Map<Integer, List<String>> formatDataCiiObject = TemplateParser.readTemplate(templateFilePath, ciiValue);
                ciiObjConfPageId = InsightWorker.createConfluencePage(gson.toJson(getStructureForConfluencePage(formatDataCiiObject, ciiValue, ciiObj.getSelfLink(), Config.getConfluenceAncestorsId(), ConfluencePageTypes.PARENT)));
                System.out.println(String.format("Template found. Created new FORMAT for %s %s: %s", simpleNameForLog, ciiValue, ciiObjConfPageId));
            } catch (IOException e) {
                System.out.println(String.format("Template was not found for %s: %s", simpleNameForLog, ciiValue));
            }
        } else {
            ciiObjConfPageId = confSearch;
            System.out.println(String.format("FORMAT for %s was found in Confluence. Fetch it", ciiValue));
        }

        if (ciiObjConfPageId != null) {
            ciiObj.setBoiFormat(ciiObjConfPageId);
            System.out.println(String.format("Updating %s %s in Insight", simpleNameForLog, ciiValue));
            InsightWorker.updateIssue(gson.toJson(ciiObj.convertToOutputInsightStructure(attributeId)), ciiObj.getInsightId());
        }
    }

    private static void checkAndFetchFormatPackage(String ciiValue, CII ciiObj, AttributeId attributeId) {
        String simpleNameForLog = ciiObj.getClass().getSimpleName();
        String confSearch = InsightWorker.findByCql(ciiValue, Config.getConfluenceSpaceKey());
        String ciiObjConfPageId = null;
        //TODO даже если родитель существует необходимо пытаться создать дочерние (с проверкой)
        if (confSearch == null) {
            System.out.println(String.format("FORMAT for %s was not found in Confluence. Check template", ciiValue));
            try {
                Map<Integer, List<String>> formatDataCiiObject = TemplateParser.readTemplate(templateFilePath, ciiValue);
                ciiObjConfPageId = InsightWorker.createConfluencePage(gson.toJson(getStructureForConfluencePage(null, ciiValue, ciiObj.getSelfLink(), Config.getConfluenceAncestorsId(), ConfluencePageTypes.PARENT)));
                System.out.println(String.format("Template found. Created new PARENT_FORMAT for %s %s: %s", simpleNameForLog, ciiValue, ciiObjConfPageId));

                List<Map<Integer, List<String>>> separateTemplates = separateMapByEmptyLine(formatDataCiiObject, Config.getFormatTemplateColumnCount());

                for (Map<Integer, List<String>> m : separateTemplates) {
                    //TODO переделать методы. Требуетс япроверка на существование страницы перед созданием
                    String childPageId = InsightWorker.createConfluencePage(gson.toJson(getStructureForConfluencePage(m, ciiValue, "", ciiObjConfPageId, ConfluencePageTypes.CHILDREN)));
                    System.out.println(String.format("Template found. Created new CHILD_FORMAT for %s %s: %s", simpleNameForLog, ciiValue, childPageId));
                }

            } catch (IOException e) {
                System.out.println(String.format("Template was not found for %s: %s", simpleNameForLog, ciiValue));
            }
        } else {
            ciiObjConfPageId = confSearch;
            System.out.println(String.format("FORMAT for %s was found in Confluence. Fetch it", ciiValue));
        }

        if (ciiObjConfPageId != null) {
            ciiObj.setBoiFormat(ciiObjConfPageId);
            System.out.println(String.format("Updating %s %s in Insight", simpleNameForLog, ciiValue));
            InsightWorker.updateIssue(gson.toJson(ciiObj.convertToOutputInsightStructure(attributeId)), ciiObj.getInsightId());
        }
    }

    /**
     * Analyze map (1 sheet of format from template) and separate it by one empty line
     *
     * @param map     input Map
     * @param columns count of columns in template
     * @return List of Maps with separated templates
     */
    private static List<Map<Integer, List<String>>> separateMapByEmptyLine(Map<Integer, List<String>> map, int columns) {
        Map<Integer, List<String>> croppedMap = cropEmptyPart(map);
        List<Map<Integer, List<String>>> templates = new ArrayList<>();
        int ch = 0;
        for (int i = 0; i < croppedMap.size(); i++) {
            if ((croppedMap.get(i).size() < columns ||
                    croppedMap.get(i).stream().distinct().limit(2).count() <= 1)
                    && i < croppedMap.size() - 1) {
                Map<Integer, List<String>> separatedTemplate = new HashMap<>();
                for (int j = ch, z = 0; j < i; j++, z++) {
                    separatedTemplate.put(z, croppedMap.get(j));
                }
                ch = i + 1;
                templates.add(separatedTemplate);
            }

            if (i == croppedMap.size() - 1 && ch < croppedMap.size()) {
                Map<Integer, List<String>> separatedMap = new HashMap<>();
                for (int j = ch, z = 0; j < croppedMap.size(); j++, z++) {
                    separatedMap.put(z, croppedMap.get(j));
                }
                templates.add(separatedMap);
            }
        }

        if (templates.isEmpty()) {
            templates.add(croppedMap);
        }
        return templates;
    }

    /**
     * Crop empty part of input map.
     *
     * @param map input Map
     * @return cropped Map
     */
    private static Map<Integer, List<String>> cropEmptyPart(Map<Integer, List<String>> map) {
        int realSize = map.size();
        for (int i = 0; i < map.size(); i++) {
            if (map.get(i).stream().distinct().limit(2).count() <= 1) {
                if (i == map.size() - 1 || map.get(i + 1).stream().distinct().limit(2).count() <= 1) {
                    realSize = i;
                    break;
                }
            }
        }

        Map<Integer, List<String>> croppedMap = new HashMap<>();
        for (int i = 0; i < realSize; i++) {
            croppedMap.put(i, map.get(i));
        }
        return croppedMap;
    }

}
