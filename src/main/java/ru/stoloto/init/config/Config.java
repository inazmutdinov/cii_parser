package ru.stoloto.init.config;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

import static ru.stoloto.init.config.InsightConfig.CONFIG_DIR;

public class Config {

    private static final String CONFIG_URL = String.format("%s/config", CONFIG_DIR);

    private static String insightUrl;
    private static String confluenceUrl;
    private static int objectSchemaId;
    private static String confluenceTemplateName;
    private static String confluenceChildTemplateName;
    private static String templateParamSheetName;
    private static String spaceKey;
    private static String ancestorsId;
    private static int formatTemplateColumnCount;
    private static String user;
    private static String password;

    static {
        try {
            Parameters params = new Parameters();
            File propertiesFile = new File(CONFIG_URL);

            FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                    new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                            .configure(params.fileBased()
                                    .setFile(propertiesFile).setEncoding("UTF-8"));

            Configuration config = builder.getConfiguration();

            insightUrl = config.getString("jira_insight_rest_url");
            objectSchemaId = config.getInt("insight_object_schema_id");
            templateParamSheetName = config.getString("template_param_sheet_name");
            confluenceTemplateName = config.getString("confluence_template_name");
            confluenceChildTemplateName = config.getString("confluence_child_template_name");
            confluenceUrl = config.getString("confluence_rest_url");
            spaceKey = config.getString("space_key");
            ancestorsId = config.getString("ancestors_id");
            formatTemplateColumnCount = config.getInt("format_template_column_count");
            user = config.getString("user");
            password = config.getString("password");
            System.out.println("'config' file was read successfully");
        } catch (ConfigurationException cex) {
            System.out.println("Error while reading 'config' file");
            cex.printStackTrace();
            System.exit(0);
        }
    }

    public static String getInsightUrl() {
        return insightUrl;
    }

    public static String getUser() {
        return user;
    }

    public static String getPassword() {
        return password;
    }

    public static int getObjectSchemaId() {
        return objectSchemaId;
    }

    public static String getConfluenceUrl() {
        return confluenceUrl;
    }

    public static String getTemplateParamSheetName() {
        return templateParamSheetName;
    }

    public static String getConfluenceTemplateName() {
        return confluenceTemplateName;
    }

    public static String getConfluenceChildTemplateName() {
        return confluenceChildTemplateName;
    }

    public static String getConfluenceSpaceKey() {
        return spaceKey;
    }

    public static String getConfluenceAncestorsId() {
        return ancestorsId;
    }

    public static int getFormatTemplateColumnCount() {
        return formatTemplateColumnCount;
    }

}
